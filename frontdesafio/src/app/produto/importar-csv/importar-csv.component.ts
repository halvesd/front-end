import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-importar-csv',
  templateUrl: './importar-csv.component.html',
  styleUrls: ['./importar-csv.component.css']
})
export class ImportarCsvComponent implements OnInit {

  baseUrl: string = environment.api + '/importacsv';

  uploadForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      profile: [null]
    });
  }

  onFileSelect($event) {
    if ($event.target.files.length > 0) {
      const file = $event.target.files[0];
      this.uploadForm.get('profile').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('arquivo', this.uploadForm.get('profile').value);

    this.httpClient.post<any>(this.baseUrl, formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
}
