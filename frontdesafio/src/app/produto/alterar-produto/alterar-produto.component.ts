import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/services/produto.service';
import { Produto } from 'src/app/model/produto';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alterar-produto',
  templateUrl: './alterar-produto.component.html',
  styleUrls: ['./alterar-produto.component.css']
})
export class AlterarProdutoComponent implements OnInit {

  inscricao: Subscription;

  id: any;

  objeto: any;

  produto: Produto;

  constructor(private router: ActivatedRoute, private produtoService: ProdutoService) {

    this.objeto = {};
  }


  ngOnInit() {
    this.inscricao = this.router.params.subscribe((params: any) => {
      // tslint:disable-next-line: no-string-literal
      this.id = params['id'];
    });

    this.produtoService.buscarPorId(this.id).subscribe(objeto => {
      this.objeto = objeto.dados;
      console.log('obj => ', objeto);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  public reset() {
    this.objeto = {};
  }

  public salvar(objeto: any) {
    if (objeto.id) {
      this.produtoService.alterar(objeto.id, objeto).subscribe(
        () => {},
      );
    }
  }
}
