import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/services/produto.service';
import { Produto } from 'src/app/model/produto';

@Component({
  selector: 'app-incluir-produto',
  templateUrl: './incluir-produto.component.html',
  styleUrls: ['./incluir-produto.component.css']
})
export class IncluirProdutoComponent implements OnInit {

  objeto: any;

  constructor(private produtoService: ProdutoService) {
    this.objeto = {};
  }

  ngOnInit() {
  }

  public reset() {
    this.objeto = {};
  }

  public salvar(objeto: any) {
    if (objeto.id) {
      this.produtoService.alterar(objeto.id, objeto).subscribe(
        () => {
          this.reset();
        },
      );
    } else {
      this.produtoService.incluir(objeto).subscribe(
        () => { },
      );
    }
  }

}
