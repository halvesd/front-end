import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/services/produto.service';
import { Produto } from 'src/app/model/produto';
import { UtilFormatacaoService } from 'src/app/services/util-formatacao.service';

@Component({
  selector: 'app-listar-produto',
  templateUrl: './listar-produto.component.html',
  styleUrls: ['./listar-produto.component.css']
})
export class ListarProdutoComponent implements OnInit {

  listaProduto: any[];

  constructor(private produtoService: ProdutoService, private utilFormatacaoService: UtilFormatacaoService) {
  }

  ngOnInit() {

    this.produtoService.listarProduto().subscribe((objeto: any) => {
      Object.values(objeto).forEach((lista: Produto[]) => {
        if (lista !== null) {
          this.listaProduto = lista;
        }
      });
    });
  }

  public excluir(id: number) {
    this.produtoService.excluir(id).subscribe(
      () => {
        this.ngOnInit();
      },
    );
  }

  public formatterDate(data: any) {
    return this.utilFormatacaoService.formatterDate(data);
  }

  public formatterValue(valor: any, digito: number) {
    return this.utilFormatacaoService.formatterValue(valor, digito);
  }
}
