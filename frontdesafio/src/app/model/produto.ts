export interface Produto {
    bandeira?: string;
    coproduto?: string;
    datacoleta?: Date;
    distribuidora?: string;
    id?: number;
    noproduto?: string;
    siglaestado?: string;
    siglamunicipio?: string;
    siglaregiao?: string;
    unidademedida?: string;
    valorcompra?: number;
    valorvenda?: number;
}
