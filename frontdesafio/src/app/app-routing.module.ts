import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { ListarProdutoComponent } from './produto/listar-produto/listar-produto.component';
import { IncluirProdutoComponent } from './produto/incluir-produto/incluir-produto.component';
import { AlterarProdutoComponent } from './produto/alterar-produto/alterar-produto.component';
import { ImportarCsvComponent } from './produto/importar-csv/importar-csv.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'produto/listar', component: ListarProdutoComponent },
  { path: 'produto/incluir', component: IncluirProdutoComponent },
  { path: 'produto/alterar/:id', component: AlterarProdutoComponent },
  { path: 'produto/importar', component: ImportarCsvComponent }
];

@NgModule({
   imports: [
      RouterModule.forRoot(routes)
   ],
   exports: [
      RouterModule
   ],
   declarations: [

   ]
})
export class AppRoutingModule { }
