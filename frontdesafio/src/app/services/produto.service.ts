import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  baseUrl: string = environment.api;

  constructor(private http: HttpClient) { }

  public incluir(model: any): Observable<any> {
    const serviceUrl: string = environment.api + `/produto`;
    return this.http.post(serviceUrl, model).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }

  public alterar(id: number, model: any): Observable<any> {
    const serviceUrl: string = environment.api + `/produto/${id}`;
    return this.http.put(serviceUrl, model).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }

  public excluir(id: number): Observable<any> {
    const serviceUrl: string = environment.api + `/produto/${id}`;
    return this.http.delete(serviceUrl).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }

  public buscarPorId(id: number): Observable<any> {
    console.log(id);
    const serviceUrl: string = environment.api + `/produto/${id}`;
    return this.http.get(serviceUrl).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }


  public listarProduto(): Observable<any> {
    const serviceUrl: string = environment.api + '/produto';
    return this.http.get(serviceUrl).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }

  public listarRegiao(): Observable<any> {
    const serviceUrl: string = environment.api + `/exibeinformacaoimportadasiglaregiao`;
    return this.http.get(serviceUrl).pipe(
      map((e: Response) => e),
      catchError((e: Response) => throwError(e))
    );
  }


}
