import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilFormatacaoService {

  constructor() { }

  public formatterValue(price: number, digito: number) {
    return new Intl.NumberFormat('pt-BR', { minimumFractionDigits: digito }).format(price);
  }

  public formatterDate(objeto: any) {
    const date = new Date(objeto);
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    const year = date.getFullYear().toString();
    return [day, month, year].join('/');
  }

}
