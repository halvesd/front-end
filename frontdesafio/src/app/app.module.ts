import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ListarProdutoComponent } from './produto/listar-produto/listar-produto.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { IncluirProdutoComponent } from './produto/incluir-produto/incluir-produto.component';
import { AlterarProdutoComponent } from './produto/alterar-produto/alterar-produto.component';
import { ImportarCsvComponent } from './produto/importar-csv/importar-csv.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ListarProdutoComponent,
    IncluirProdutoComponent,
    AlterarProdutoComponent,
    ImportarCsvComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  exports: [BsDatepickerModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
  ]
})
export class AppModule { }
